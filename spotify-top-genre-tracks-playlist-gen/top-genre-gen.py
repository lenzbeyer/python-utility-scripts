import json
import spotipy
from spotipy.oauth2 import SpotifyOAuth
import os
import time
import random
import argparse

# Load credentials from config.json
config_path = os.path.join(os.path.dirname(__file__), "config.json")
with open(config_path) as config_file:
    config = json.load(config_file)

CLIENT_ID = config["client_id"]
CLIENT_SECRET = config["client_secret"]
REDIRECT_URI = config["redirect_uri"]

# Set up authentication
sp = spotipy.Spotify(
    auth_manager=SpotifyOAuth(
        client_id=CLIENT_ID,
        client_secret=CLIENT_SECRET,
        redirect_uri=REDIRECT_URI,
        scope="playlist-modify-public",
    )
)


def get_all_genres(file_path):
    # Read genres from a text file
    with open(file_path, "r") as file:
        genres = [line.strip() for line in file.readlines()]
    return genres


def get_top_track_for_genre(genre):
    # Search for multiple tracks within the genre and select the one with the highest popularity
    results = sp.search(q=f"genre:{genre}", type="track", limit=50)
    tracks = results["tracks"]["items"]
    if tracks:
        top_track = max(tracks, key=lambda track: track["popularity"])
        track_popularity = top_track["popularity"]
        print(f"Top track for genre {genre} popularity is {track_popularity}.")
        return top_track["id"]
    return None


def create_playlist(name, description):
    user_id = sp.current_user()["id"]
    playlist = sp.user_playlist_create(
        user_id, name, public=True, description=description
    )
    return playlist["id"]


def add_track_to_playlist(playlist_id, track_id):
    # Check if the track is already in the playlist
    existing_tracks = sp.playlist_tracks(playlist_id)
    existing_track_ids = [item["track"]["id"] for item in existing_tracks["items"]]
    if track_id not in existing_track_ids:
        sp.playlist_add_items(playlist_id, [track_id])


def main():
    # Set up command-line argument parsing
    parser = argparse.ArgumentParser(
        description="Create a Spotify playlist with top tracks from various genres."
    )
    parser.add_argument(
        "genre_file", type=str, help="Path to the text file containing genre labels."
    )
    args = parser.parse_args()

    # Extract the filename without the extension and convert to title case
    filename = os.path.basename(args.genre_file)
    filename_without_extension = os.path.splitext(filename)[0]
    formatted_filename = filename_without_extension.replace("_", " ").title()

    # Get genres from the provided text file
    genres = get_all_genres(args.genre_file)
    print(f"Found {len(genres)} genres.")

    playlist_name = f"{formatted_filename} Top Tracks"
    playlist_description = (
        f"A playlist with the top tracks from {formatted_filename} genres."
    )
    playlist_id = create_playlist(playlist_name, playlist_description)

    for genre in genres:
        print(f"Finding top track for genre: {genre}")
        top_track_id = get_top_track_for_genre(genre)
        if top_track_id:
            add_track_to_playlist(playlist_id, top_track_id)
            print(f"Added top track for genre {genre} to playlist.")

        # Randomized delay between 0.5 and 1.5 seconds
        time.sleep(random.uniform(0.5, 1.0))

    print(
        f"Playlist '{playlist_name}' created and populated with top tracks from each genre."
    )


if __name__ == "__main__":
    main()
