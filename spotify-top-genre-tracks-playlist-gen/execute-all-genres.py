import os
import subprocess

# Get the directory where the script is located
script_dir = os.path.dirname(os.path.abspath(__file__))

# List all files in the directory
files_in_directory = os.listdir(script_dir)

# Filter the list to include only .txt files
txt_files = [f for f in files_in_directory if f.endswith('.txt')]

# Iterate over each .txt file and execute the PowerShell command
for txt_file in txt_files:
    # Create the command
    command = f'py top-genre-gen.py "{txt_file}"'
    
    # Execute the command using PowerShell
    subprocess.run(["powershell", "-Command", command], check=True)
