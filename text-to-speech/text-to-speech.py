import os
import argparse
import markdown
from gtts import gTTS


def text_to_speech(text, output_file):
    # Initialize gTTS (Google Text-to-Speech) with English language
    print(text)
    tts = gTTS(text=text, lang="en")

    # Save the speech to a WAV file
    tts.save(output_file)


def read_markdown_file(markdown_file):
    with open(markdown_file, "r", encoding="utf-8") as f:
        md_text = f.read()
        return md_text


if __name__ == "__main__":
    # Command-line arguments
    parser = argparse.ArgumentParser(
        description="Convert Markdown text to speech and save as WAV file."
    )
    parser.add_argument(
        "input_file",
        metavar="input_file",
        type=str,
        help="path to the input Markdown file",
    )
    parser.add_argument(
        "output_file", metavar="output_file", type=str, help="output WAV file name"
    )

    args = parser.parse_args()

    # Read Markdown file content
    text = read_markdown_file(args.input_file)

    # Convert plain text to speech and save as WAV file
    text_to_speech(text, args.output_file)

    print(f"Text converted to speech and saved as {args.output_file}")
