import os
import sys


def rename_wav_files(mp3_folder: str, wav_folder: str):
    if not os.path.isdir(mp3_folder) or not os.path.isdir(wav_folder):
        print("Error: One or both specified paths are not valid directories.")
        return

    mp3_subfolders = [
        f for f in os.listdir(mp3_folder) if os.path.isdir(os.path.join(mp3_folder, f))
    ]
    wav_subfolders = [
        f for f in os.listdir(wav_folder) if os.path.isdir(os.path.join(wav_folder, f))
    ]

    for subfolder in mp3_subfolders:
        mp3_sub_path = os.path.join(mp3_folder, subfolder)
        wav_sub_path = os.path.join(wav_folder, subfolder)

        if subfolder in wav_subfolders:
            mp3_files = sorted(
                [f for f in os.listdir(mp3_sub_path) if f.lower().endswith(".mp3")]
            )
            wav_files = sorted(
                [f for f in os.listdir(wav_sub_path) if f.lower().endswith(".wav")]
            )

            if len(mp3_files) == len(wav_files):
                for mp3_file, wav_file in zip(mp3_files, wav_files):
                    mp3_name, _ = os.path.splitext(mp3_file)
                    wav_extension = os.path.splitext(wav_file)[1]
                    old_wav_path = os.path.join(wav_sub_path, wav_file)
                    new_wav_path = os.path.join(wav_sub_path, mp3_name + wav_extension)

                    os.rename(old_wav_path, new_wav_path)
                    print(f"Renamed: {wav_file} -> {mp3_name + wav_extension}")
            else:
                print(f"Mismatch in file count for folder: {subfolder}")
        else:
            print(f"No matching folder found for: {subfolder}")


if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage: python script.py <mp3_folder> <wav_folder>")
    else:
        rename_wav_files(sys.argv[1], sys.argv[2])
