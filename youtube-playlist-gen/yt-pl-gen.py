import os
import argparse
import google_auth_oauthlib.flow
import googleapiclient.discovery
import googleapiclient.errors

# Set up YouTube API
scopes = ["https://www.googleapis.com/auth/youtube.force-ssl"]


def youtube_authenticate():
    # Disable OAuthlib's HTTPS verification when running locally.
    # *DO NOT* leave this option enabled in production.
    os.environ["OAUTHLIB_INSECURE_TRANSPORT"] = "1"

    api_service_name = "youtube"
    api_version = "v3"
    client_secrets_file = "config.json"

    # Get credentials and create an API client
    flow = google_auth_oauthlib.flow.InstalledAppFlow.from_client_secrets_file(
        client_secrets_file, scopes
    )
    credentials = flow.run_local_server(port=0)
    return googleapiclient.discovery.build(
        api_service_name, api_version, credentials=credentials
    )


def search_video(youtube, query):
    request = youtube.search().list(
        part="snippet", maxResults=1, q=query + " instrumental"
    )
    response = request.execute()
    return response["items"][0]["id"]["videoId"] if response["items"] else None


def create_playlist(youtube, title, description):
    request = youtube.playlists().insert(
        part="snippet,status",
        body={
            "snippet": {
                "title": title,
                "description": description,
                "tags": ["instrumental", "music", "playlist"],
                "defaultLanguage": "en",
            },
            "status": {"privacyStatus": "public"},
        },
    )
    response = request.execute()
    return response["id"]


def add_video_to_playlist(youtube, playlist_id, video_id):
    request = youtube.playlistItems().insert(
        part="snippet",
        body={
            "snippet": {
                "playlistId": playlist_id,
                "resourceId": {"kind": "youtube#video", "videoId": video_id},
            }
        },
    )
    request.execute()


def load_songs_from_file(songs_file):
    if not os.path.exists(songs_file):
        raise FileNotFoundError(f"File {songs_file} does not exist.")
    with open(songs_file, "r") as file:
        songs = [line.strip() for line in file if line.strip()]
    return songs


def main():
    # Set up argument parser
    parser = argparse.ArgumentParser(
        description="Create a YouTube playlist from a list of songs."
    )
    parser.add_argument("songs_file", type=str, help="Path to the songs .txt file")
    args = parser.parse_args()

    youtube = youtube_authenticate()

    # Load songs from the file
    try:
        songs = load_songs_from_file(args.songs_file)
    except FileNotFoundError as e:
        print(e)
        return

    # Extract the filename without extension
    filename = os.path.splitext(os.path.basename(args.songs_file))[0]

    # Create a new playlist
    playlist_id = create_playlist(
        youtube,
        f"{filename} Instrumentals",
        f"Instrumental versions of the {filename}",
    )

    # Search for each song and add the instrumental version to the playlist
    for song in songs:
        video_id = search_video(youtube, song)
        if video_id:
            add_video_to_playlist(youtube, playlist_id, video_id)
            print(f"Added {song} instrumental to the playlist")
        else:
            print(f"Instrumental version of {song} not found")


if __name__ == "__main__":
    main()
