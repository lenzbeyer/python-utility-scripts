# Python Utility Scripts

## Description
This repository contains python utility scripts for various tasks. Most have been created using ChatGPT.

## Installation
Before executing the scripts, it is advisable to set up an anaconda virtual environment for python. This way, all installed dependencies of a project can be encapsulated in a virtual environment to make shure dependencies of different projects are not interfering with one another. Use this (tutorial)[https://www.geeksforgeeks.org/set-up-virtual-environment-for-python-using-anaconda/] to set up the environment.

After that, install the dependencies of the chosen script using pip and [chocolatey]((https://chocolatey.org/install)).

### Assembly AI Speech To Text
This script processes video files by extracting their audio, transcribing the audio using AssemblyAI, and saving the transcription to a Markdown file. The script accepts a single video file or a directory of video files as input, and it requires an AssemblyAI API key specified in a configuration file (`config.json`). It supports German language transcription and organizes output transcriptions into a specified output folder.

```
pip install requests
pip install moviepy
pip install pydub
pip install assemblyai
```

### Audiobook splitter
This script splits an audio file into 30-minute segments and saves the segments as separate MP3 files. It takes an input audio file and an optional output directory for saving the segmented files. If the output directory does not exist, it is created automatically.

```
pip install pydub

choco install ffmpeg
```

### Puny Puny Puns book generator
This script fetches and processes Mastodon toots (posts) from my puny puny puns accunt, to generate a pdf gift book.
```
pip install mastodon.py
pip install beautifulsoup4
pip install reportlab
```

### Spotify playlist generator
This script utilizes the Spotify API to create a playlist titled "Top Tracks by Artists", containing the top track from each of the listed jazz artists. This is usefull to for example create a playlist to discover artist playing at a festival based on the flyer.
```
pip install spotipy
```

### Spotify Top Track by Genre playlist generator

This script leverages the Spotify API to create a public playlist featuring the top tracks from various genres listed in a text file. It dynamically searches for each genre's top track based on popularity, adds them to the newly created playlist, and handles OAuth authentication via configuration file integration. The genre tags where manually extracted from the [list of music genres at Chosic](https://www.chosic.com/list-of-music-genres/).

```
pip install spotipy
```

### Text To Speech with Google Translate
This script converts Markdown text into speech and saves it as a WAV file using the gTTS (Google Text-to-Speech) library. It accepts a Markdown file as input and generates a corresponding audio file, making it suitable for creating audio versions of Markdown documents.

```
pip install markdown
pip install gtts
```

### Youtube Instrumentals playlist generator
This script utilizes the YouTube Data API v3 to create a public playlist from a list of songs specified in a text file. It authenticates via OAuth, searches for instrumental versions of each song, and adds them to the playlist.

```
import os
import argparse
import google_auth_oauthlib.flow
import googleapiclient.discovery
import googleapiclient.errors
```

## License
This repository is open soruce under [MIT License](https://opensource.org/license/mit/).
