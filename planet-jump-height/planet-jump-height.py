import math

# Gravitational accelerations (m/s^2) on various celestial bodies
gravity = {
    "Mercury": 3.7,
    "Venus": 8.87,
    "Earth": 9.81,
    "Moon": 1.62,
    "Mars": 3.71,
    "Vesta": 0.22,
    # "Jupiter": 24.79,
    # "Saturn": 10.44,
    # "Uranus": 8.69,
    # "Neptune": 11.15,
    "Pluto": 0.62,
    # "Ganymede": 1.43,
    # "Callisto": 1.24,
    # "Io": 1.8,
    # "Europa": 1.31,
    "Titan": 1.35,
    # "Triton": 0.78,
    # "Enceladus": 0.113,
    # "Mimas": 0.064,
    # "Rhea": 0.264,
    # "Dione": 0.232,
    # "Tethys": 0.145,
    "Iapetus": 0.223,
}


def jump_height(earth_jump_height, gravity_on_body):
    """
    Calculate the jump height on another celestial body.
    :param earth_jump_height: Height jumped on Earth (meters)
    :param gravity_on_body: Gravitational acceleration on the celestial body (m/s^2)
    :return: Jump height on the celestial body (meters)
    """
    return earth_jump_height * (9.81 / gravity_on_body)


def jump_distance(earth_jump_distance, gravity_on_body):
    """
    Calculate the jump distance on another celestial body.
    :param earth_jump_distance: Distance jumped on Earth (meters)
    :param gravity_on_body: Gravitational acceleration on the celestial body (m/s^2)
    :return: Jump distance on the celestial body (meters)
    """
    return earth_jump_distance * (9.81 / gravity_on_body)


def calculate_jump_height(weight_kg, force_n=1500):
    # Constants
    g = 9.81  # Acceleration due to gravity on Earth in m/s^2

    # Calculate the mass in kg
    mass = weight_kg  # Mass in kg (numerically equal to the weight)

    # Assume that the distance over which the force is applied is small (let's assume 0.2 meters)
    # Work done = Force * Distance
    distance_of_force_application = 0.21  # meters

    # Work done = Kinetic Energy = Force * Distance
    work_done = force_n * distance_of_force_application

    # This work is converted to Potential Energy at the peak of the jump
    # Potential Energy = m * g * h
    # Solving for h: h = work_done / (mass * g)
    jump_height = work_done / (mass * g)  # Jump height in meters

    return jump_height


def calculate_jump_distance(weight_kg, force_n=1500):
    time_of_force_application = 0.15  # Time duration of force application (seconds)

    # Calculate acceleration
    mass = weight_kg
    acceleration = force_n / mass

    # Calculate initial vertical speed
    initial_vertical_speed = acceleration * time_of_force_application
    print(
        f"The initial vertical speed is approximately {initial_vertical_speed:.2f} m/s."
    )

    # Input parameters (these can be adjusted)
    initial_horizontal_speed = 2.5  # Initial horizontal speed (m/s)

    # Calculate the time to reach the peak
    time_up = initial_vertical_speed / gravity["Earth"]

    # Total time in the air
    total_time_in_air = 2 * time_up

    # Calculate the horizontal distance (jump distance)
    return initial_horizontal_speed * total_time_in_air


# Print jump heights and distances on each celestial body
# Constants
mass = 80 + 140  # Mass of the person (kg) +  Space Suit
earth_jump_height = calculate_jump_height(mass)
earth_jump_distance = calculate_jump_distance(mass)
print(
    f"Based on a force of 1500 N and a weight of {mass}kg, the estimated jump height on earth is {earth_jump_height:.2f} meters.\n"
)

force_applied = 1500  # Force applied during the jump (N)


print(
    f"The average jump distance of a person weighing {mass} kg on Earth is approximately {earth_jump_distance:.2f} meters.\n"
)


print(f"{'Celestial Body':<15} {'Jump Height (m)':<20} {'Jump Distance (m)':<20}")
print("-" * 55)
for body, g in gravity.items():

    height_on_body = jump_height(earth_jump_height, g)
    distance_on_body = jump_distance(earth_jump_distance, g)
    print(f"{body:<15} {height_on_body:<20.2f} {distance_on_body:<20.2f}")
