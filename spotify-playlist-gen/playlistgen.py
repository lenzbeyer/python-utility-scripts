import spotipy
from spotipy.oauth2 import SpotifyOAuth
import json
import os
import sys

def load_artists(file_path):
    with open(file_path, 'r') as file:
        return [line.strip() for line in file]

# config.json
# {
#     "client_id": "YOUR_CLIENT_ID",
#     "client_secret": "YOUR_CLIENT_SECRET",
#     "redirect_uri": "YOUR_REDIRECT_URI"
# }
def load_config(config_path):
    with open(config_path, 'r') as file:
        return json.load(file)

def main(artists_file_path):
    script_dir = os.path.dirname(os.path.abspath(__file__))
    input_folder = os.path.join(script_dir, 'input')
    
    # Load artists from file
    artists_file_path = artists_file_path or os.path.join(input_folder, 'artists.txt')
    artists = load_artists(artists_file_path)
    
    # Load Spotify credentials from config.json
    config_path = os.path.join(script_dir, 'config.json')
    config = load_config(config_path)

    # Initialize the Spotify API client
    sp = spotipy.Spotify(auth_manager=SpotifyOAuth(client_id=config['client_id'],
                                                   client_secret=config['client_secret'],
                                                   redirect_uri=config['redirect_uri'],
                                                   scope='playlist-modify-public'))

    # Create a new playlist
    playlist = sp.user_playlist_create(sp.current_user()['id'], 'Top Tracks by Artists')

    # Iterate through the list of artists and add their top track to the playlist
    for artist_name in artists:
        results = sp.search(q=f'artist:"{artist_name}"', type='artist')
        if results['artists']['items']:
            artist = results['artists']['items'][0]
            top_tracks = sp.artist_top_tracks(artist['id'])
            if top_tracks['tracks']:
                top_track = top_tracks['tracks'][0]
                sp.playlist_add_items(playlist['id'], [top_track['uri']])

    print(f'Playlist "{playlist["name"]}" created with the top tracks of the listed artists.')

if __name__ == "__main__":
    artists_file_path = sys.argv[1] if len(sys.argv) > 1 else None
    main(artists_file_path)
