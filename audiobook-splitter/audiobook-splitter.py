import os
import argparse
from pydub import AudioSegment


def split_audio(input_file, output_dir):
    audio = AudioSegment.from_file(input_file)
    segment_length = 30 * 60 * 1000  # 30 minutes in milliseconds

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    for i, start_time in enumerate(range(0, len(audio), segment_length)):
        end_time = start_time + segment_length
        segment = audio[start_time:end_time]
        output_file = os.path.join(
            output_dir, f"{os.path.basename(input_file)[:-4]}_part_{i + 1}.mp3")
        segment.export(output_file, format="mp3")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Split an audio file into 30-minute segments.")
    parser.add_argument("input_file", help="Path to the input audio file.")
    parser.add_argument("--output-dir", default="output",
                        help="Directory to save the segmented audio files.")
    args = parser.parse_args()

    input_file = args.input_file
    output_dir = args.output_dir

    split_audio(input_file, output_dir)
