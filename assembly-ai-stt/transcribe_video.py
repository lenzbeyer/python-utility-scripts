import sys
import time
import requests
import moviepy.editor as mp
from pydub import AudioSegment
import json
import os
import assemblyai as aai

## config.json example format:
# {
#     "assemblyai_api_key": "<your key>"
# }

CONFIG_FILE = "config.json"
ASSEMBLYAI_API_URL = "https://api.assemblyai.com/v2"


def load_config():
    with open(CONFIG_FILE, "r") as f:
        return json.load(f)


def extract_audio_from_video(video_path, audio_path):
    video = mp.VideoFileClip(video_path)
    video.audio.write_audiofile(audio_path)


def transcribe_audio_with_assemblyai(audio_url, api_key):
    aai.settings.api_key = api_key
    config = aai.TranscriptionConfig(language_code="de")

    transcriber = aai.Transcriber(config=config)
    transcript = transcriber.transcribe(audio_url)

    if transcript.status == aai.TranscriptStatus.error:
        return transcript.error
    else:
        return transcript.text


def process_video_file(video_path, filename, api_key):
    audio_path = "extracted_audio.mp3"
    filename_without_extension, _ = os.path.splitext(filename)

    output_foldername = "output"
    if not os.path.exists(output_foldername):
        os.makedirs(output_foldername)

    output_transcription_path = f"./{output_foldername}/{filename_without_extension}.md"

    # Load config
    config = load_config()
    api_key = config["assemblyai_api_key"]

    # Extract audio from video
    extract_audio_from_video(video_path, audio_path)

    # Transcribe the audio with AssemblyAI
    transcription = transcribe_audio_with_assemblyai(audio_path, api_key)

    # Save transcription to a Markdown file
    with open(output_transcription_path, "w", encoding="utf-8") as md_file:
        md_file.write(transcription)

    print(f"Transcription saved to {output_transcription_path}")


def main():
    if len(sys.argv) != 2:
        print("Usage: python transcribe_video.py <path_to_video_or_folder>")
        sys.exit(1)

    path = sys.argv[1]

    # Load config
    config = load_config()
    api_key = config["assemblyai_api_key"]

    if os.path.isfile(path):
        process_video_file(path, api_key)
    elif os.path.isdir(path):
        for filename in os.listdir(path):
            if filename.lower().endswith(".mp4"):
                video_path = os.path.join(path, filename)
                process_video_file(video_path, filename, api_key)
    else:
        print(f"Invalid path: {path}")
        sys.exit(1)


if __name__ == "__main__":
    main()
